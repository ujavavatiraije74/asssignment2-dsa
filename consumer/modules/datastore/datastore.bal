import consumer.model as Model;
import ballerinax/mongodb;

public class Store {
    private mongodb:Client mongoClient;
    private string collection;

    public function init(Model:Configuration configuration) {
        mongodb:ConnectionConfig mongoConfig = {
            host: configuration.host,
            port: configuration.port
        };
        self.mongoClient = checkpanic new (mongoConfig, configuration.database);
        self.collection = configuration.collection;
    }

    # Description
    # Logs the customer into the system
    # + id - id of the customer
    # + password - password of the customer
    # + return - Returns a boolean if the customer exists or not, or returns an error
    public isolated function login(string id, string password) returns boolean|error {
        Model:CustomerQuery customer = {
            id: id,
            password: password
        
        };

        stream<Model:Customer, error?> result = checkpanic self.mongoClient->find(self.collection, (), customer, 'limit = 1);

        record {|Model:Customer value;|}|error? res = result.next();

        while (res is record {|Model:Customer value;|}) {
            Model:Customer cust = res.value;
            if (id == cust.id && password == cust.password) {
                return true;
            }
        }
        return error("Invalid login credentials");
    }


    # Description
    # Retrieves all products from the database
    # + return - Returns a stream of products or an error
    public isolated function allProducts() returns Model:Product[] {
        stream<Model:Product, error?> products = checkpanic self.mongoClient->find("Product", (), ());
        Model:Product[] product = [];

        record {|Model:Product value;|}|error? res = products.next();

        while (res is record {|Model:Product value;|}) {
            product.push(res.value);
            res = products.next();
        }

        return product;
    }

    
    public function saveOrder(Model:OrderConsumerRecord customerOrder) {
        
        map<json> incomingOrder = <map<json>>customerOrder.value.toJson();
    }


}
