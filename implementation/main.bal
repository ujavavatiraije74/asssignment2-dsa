import ballerina/io;
import implementation.datastore as Datastore;
import implementation.model as Model;
import ballerinax/kafka;
import implementation.util as Util;

kafka:Consumer kafkaConsumer = check new (kafka:DEFAULT_URL, Model:consumerConfigs);
listener kafka:Listener Listener = new (kafka:DEFAULT_URL, Model:consumerConfigs);

service /kafka on Listener {
    private Datastore:Store store;
    private Model:Product[] productList;
    private Model:Configuration config = {
        host: "localhost",
        port: 27017,
        database: "store",
        collection: "Customer"
    };

    function init() {
        self.store = new (self.config);
        self.productList = self.store.allProducts();
    }

    string id = io:readln("Enter your Account ID Number : ");
    string password = io:readln("Enter your Password : ");
    boolean keepOrdering = true;

    int i = 1;
    int j = 1;

    function processOrder(kafka:ConsumerRecord consumerRecord, Datastore:Store store) returns error|error {

        string id = io:readln("Enter your Account ID Number : ");
        string password = io:readln("Enter your Password : ");
        boolean keepOrdering = true;
        string[] products = [];
        int j = 1;
        Model:Product product;
        decimal totalPrice = 0.0;
        int totalQuantity = 0;
        if (id == "" || password == "") {
            io:println("Please enter your Account ID Number and Password");
            return error("Please enter your Account ID Number and Password");
        } else if (check self.store.login(id, password)) {
            io:println("Login Successful");

            io:println("Welcome to the Store");
            io:println("Select the item you wish to purchase");

            foreach var item in self.productList {
                io:println(string `Product Name : ${item.name} -> ${item.id}`);
            }

            while (keepOrdering) {
                string productIdChoice = io:readln("Enter the Product Number : ");

                Model:Product|error productChoice = self.store.findSingleProduct(productIdChoice);

                if (productChoice is error) {
                    io:println("Invalid Choice");
                } else {
                    int|error quantity = check int:fromString(io:readln("Enter the quantity : "));

                    if quantity is int {
                        if quantity > self.productList.length() {
                            io:println("The quantity you have entered is not available! Please Pick an option to continue");
                            io:println("1) Adjust Quantity");
                            io:println("2) Remove Item");
                            io:println("3) Cancel Order");

                            string option = io:readln("Enter your option : ");
                            if option == "1" {
                                quantity = check int:fromString(io:readln("Enter the quantity : "));
                                if quantity is error {
                                    io:println("Invalid Quantity Try Again");
                                } else {
                                    products.push(productIdChoice + " : " + quantity.toString());
                                }
                            } else if option == "2" {
                                products = products.filter(i => i != productIdChoice + " : " + check quantity.toString());
                            } else if option == "3" {
                                return error("Order Cancelled");
                            } else {
                                io:println("Invalid Option");
                            }
                        }
                    } else {
                        io:println("Invalid quantity");
                    }

                    string keepOrderingString = io:readln("Do you want to place another order? (y/n) : ");
                    if (keepOrderingString == "n") {
                        keepOrdering = false;
                    }
                }
            }

            io:println(string `Your total is N$ ${totalPrice} and you have ordered ${totalQuantity} items`);
            string confirmation = io:readln("Do you want to confirm your order? (y/n) : ");

            if (confirmation == "y") {
                Model:Order customerOrder = {
                id: check Util:generateOrderID(id, "", 0),
                customerId: id,
                products: products,
                totalQuantity: totalQuantity,
                totalPrice: totalPrice,
                status: "PLACED"
            };

                error? saveError = check store.saveOrder(customerOrder);
                if (saveError is error) {
                    io:println("Order Placing Failed");
                } else {
                    io:println("Order Placed Successfully");
                }

            }

        } else {
            io:println("Login Failed");
            return error("Login Failed");
        }
        
        
        return error("Order Processed");

    }
    remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] consumerRecord) returns error? {
        foreach kafka:ConsumerRecord item in consumerRecord {
            error orderResults = self.processOrder(item, self.store);
        }
    }
}
