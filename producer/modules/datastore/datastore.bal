import producer.model as Model;
import ballerinax/mongodb;

public class Store {
    private mongodb:Client mongoClient;
    private string collection;

    public function init(Model:Configuration configuration) {
        mongodb:ConnectionConfig mongoConfig = {
            host: configuration.host,
            port: configuration.port
        };
        self.mongoClient = checkpanic new (mongoConfig, configuration.database);
        self.collection = configuration.collection;
    }


    public isolated function login(string id, string password) returns boolean|error {
        Model:CustomerQuery customer = {
            id: id,
            password: password
        
        };

        stream<Model:Customer, error?> result = checkpanic self.mongoClient->find(self.collection, (), customer, 'limit = 1);

        record {|Model:Customer value;|}|error? res = result.next();

        while (res is record {|Model:Customer value;|}) {
            Model:Customer cust = res.value;
            if (id == cust.id && password == cust.password) {
                return true;
            }
        }
        return false;
    }
    
    public isolated function allProducts() returns Model:Product[] {
        stream<Model:Product, error?> products = checkpanic self.mongoClient->find("Product", (), ());
        Model:Product[] product = [];

        record {|Model:Product value;|}|error? res = products.next();

        while (res is record {|Model:Product value;|}) {
            product.push(res.value);
            res = products.next();
        }

        return product;
    }

    
    public isolated function saveOrder(Model:Order customerOrder) returns error? {
        checkpanic self.mongoClient->insert(customerOrder, "Order");
    }


}
