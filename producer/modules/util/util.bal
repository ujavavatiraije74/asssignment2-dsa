import ballerina/random;
public isolated function generateOrderID(string customerid, string storeid, int storeOrder) returns string|error{
    int randomNumber = check random:createIntInRange(1000, 9999);
    return string`R${storeid}-${customerid}-${storeOrder}-${randomNumber}`;
}