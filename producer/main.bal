import ballerinax/kafka;
import producer.datastore as Datastore;
import ballerina/io;
import producer.model as Model;
import producer.util as Util;

kafka:ProducerConfiguration producerConfig = {
    clientId: "order-producer",
    acks: "all",
    retryCount: 3
};

kafka:Producer productProducer = check new (kafka:DEFAULT_URL, producerConfig);

public function main() returns error? {
    Model:Configuration config = {
        host: "localhost",
        port: 27017,
        database: "store",
        collection: "Customer"
    };

    Datastore:Store store = new (config);

    string id = io:readln("Enter your Account ID Number : ");
    string password = io:readln("Enter your Password : ");
    boolean keepOrdering = true;
    string[] products = [];
    decimal totalPrice = 0.0;
    int totalQuantity = 0;
    Model:Product[] productList = [];
 
    if (check store.login(id, password)) {
        io:println("Login Successful");
        io:println("Welcome to the Store");
        io:println("Select the store you would like to order from");

        while (keepOrdering) {
            string productId = io:readln("Enter the product ID : ");
            int|error quantity = check int:fromString(io:readln("Enter the quantity : "));

            if quantity is error {
                io:println("Invalid quantity");
            } else {
                products.push(productId + " : " + quantity.toString());
            }

            string keepOrderingString = io:readln("Do you want to place another order? (y/n) : ");
            if (keepOrderingString == "n") {
                keepOrdering = false;
            }
        }

        string confirmation = io:readln("Do you want to confirm your order? (y/n) : ");
        if (confirmation == "y") {
            Model:Order customerOrder = {
                id: check Util:generateOrderID(id, "", 0),
                customerId: id,
                products: products,
                totalQuantity: totalQuantity,
                totalPrice: totalPrice,
                status: "PLACED"
            };
            
            check store.saveOrder(customerOrder);
            io:println("Order Placed");

        } else {
            io:println("Login Failed");
        }

    }

